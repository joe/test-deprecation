// pattern: g++ 7.4, 8.3
//   *: warning: 'S' is deprecated \[-Wdeprecated-declarations\]
//    S s;
//      ^
//   *: note: declared here
//    struct \[\[deprecated\]\] S {};
//                          ^
// pattern: g++ 9.2
//   *: warning: 'S' is deprecated \[-Wdeprecated-declarations\]
//   *( )+([0-9]) | S s;
//         |   ^
//   *: note: declared here
//   *( )+([0-9]) | struct \[\[deprecated\]\] S {};
//         |                       ^
// pattern: clang++ 6.0, 7.0, 8.0
//   *: warning: 'S' is deprecated \[-Wdeprecated-declarations\]
//   S s;
//   ^
//   *: note: 'S' has been explicitly marked deprecated here
//   struct \[\[deprecated\]\] S {};
//            ^
//   1 warning generated.

struct [[deprecated]] S {};
S s;
