// Note: the below message appears twice in g++ 8.3, we accept that by the
// first * (which usually is only supposed to match the file name

// pattern: g++ 7.4, 8.3
//   *: warning: 'void f()' is deprecated \[-Wdeprecated-declarations\]
//    auto g = &f;
//              ^
//   *: note: declared here
//    \[\[deprecated\]\] void f();
//                        ^
// pattern: g++ 9.2
//   *: warning: 'void f()' is deprecated \[-Wdeprecated-declarations\]
//   *( )+([0-9]) | auto g = &f;
//         |           ^
//   *: note: declared here
//   *( )+([0-9]) | \[\[deprecated\]\] void f();
//         |                     ^
// pattern: clang++ 6.0, 7.0, 8.0
//   *: warning: 'f' is deprecated \[-Wdeprecated-declarations\]
//   auto g = &f;
//             ^
//   *: note: 'f' has been explicitly marked deprecated here
//   \[\[deprecated\]\] void f();
//     ^
//   1 warning generated.

[[deprecated]] void f();
auto g = &f;
