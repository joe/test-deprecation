// Note: the below message appears thrice in g++ 8.3, we accept that by the
// first * (which usually is only supposed to match the file name

// pattern: g++ 7.4, 8.3
//   *: warning: 'S::x' is deprecated \[-Wdeprecated-declarations\]
//    int x = S{}.x;
//                ^
//   *: note: declared here
//      \[\[deprecated\]\] int x;
//                         ^
// pattern: g++ 9.2
//   *: warning: 'S::x' is deprecated \[-Wdeprecated-declarations\]
//   *( )+([0-9]) | int x = S{}.x;
//         |             ^
//   *: note: declared here
//   *( )+([0-9]) |   \[\[deprecated\]\] int x;
//         |                      ^
// pattern: clang++ 6.0, 7.0, 8.0
//   *: warning: 'x' is deprecated \[-Wdeprecated-declarations\]
//   int x = S{}.x;
//               ^
//   *: note: 'x' has been explicitly marked deprecated here
//     \[\[deprecated\]\] int x;
//       ^
//   1 warning generated.

struct S {
  [[deprecated]] int x;
};
int x = S{}.x;
