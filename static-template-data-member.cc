// pattern: g++ 7.4, 8.3
//   *: warning: 'template<int <anonymous> > int S::x<<anonymous> >' is deprecated \[-Wdeprecated-declarations\]
//    int &ref = S::x<i>;
//                  ^
//   *: note: declared here
//      \[\[deprecated\]\] static int x;
//                                ^
// pattern: g++ 9.2
//   *: warning: 'template<int <anonymous> > int S::x<<anonymous> >' is deprecated \[-Wdeprecated-declarations\]
//   *( )+([0-9]) | int &ref = S::x<i>;
//         |               ^
//   *: note: declared here
//   *( )+([0-9]) |   \[\[deprecated\]\] static int x;
//         |                             ^
// anti-pattern: clang++ 6.0, 7.0, 8.0

struct S {
  template<int>
  [[deprecated]] static int x;
};
template<int i>
int &ref = S::x<i>;
