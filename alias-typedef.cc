// pattern: g++ 7.4, 8.3
//   *: warning: 'using S = int' is deprecated \[-Wdeprecated-declarations\]
//    typedef S Alias;
//              ^~~~~
//   *: note: declared here
//    using S \[\[deprecated\]\] = int;
//                                ^
// pattern: g++ 9.2
//   *: warning: 'using S = int' is deprecated \[-Wdeprecated-declarations\]
//   *( )+([0-9]) | typedef S Alias;
//         |           ^~~~~
//   *: note: declared here
//   *( )+([0-9]) | using S \[\[deprecated\]\] = int;
//         |       ^
// pattern: clang++ 6.0, 7.0, 8.0
//   *: warning: 'S' is deprecated \[-Wdeprecated-declarations\]
//   typedef S Alias;
//           ^
//   *: note: 'S' has been explicitly marked deprecated here
//   using S \[\[deprecated\]\] = int;
//             ^
//   1 warning generated.

using S [[deprecated]] = int;
typedef S Alias;
