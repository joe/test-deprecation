// pattern: g++ 7.4, 8.3
//   *: warning: 'E' is deprecated \[-Wdeprecated-declarations\]
//    typedef E S;
//              ^
//   *: note: declared here
//    enum \[\[deprecated\]\] E {};
//                        ^
// pattern: g++ 9.2
//   *: warning: 'E' is deprecated \[-Wdeprecated-declarations\]
//   *( )+([0-9]) | typedef E S;
//         |           ^
//   *: note: declared here
//   *( )+([0-9]) | enum \[\[deprecated\]\] E {};
//         |                     ^
// pattern: clang++ 6.0, 7.0, 8.0
//   *: warning: 'E' is deprecated \[-Wdeprecated-declarations\]
//   typedef E S;
//           ^
//   *: note: 'E' has been explicitly marked deprecated here
//   enum \[\[deprecated\]\] E {};
//          ^
//   1 warning generated.

enum [[deprecated]] E {};
typedef E S;
