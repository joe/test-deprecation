// pattern: g++ 7.4, 8.3
//   *: warning: 'x<0>' is deprecated \[-Wdeprecated-declarations\]
//    int &ref = x<0>;
//               ^~~~
//   *: note: declared here
//    \[\[deprecated\]\] int x;
//                       ^
// pattern: g++ 9.2
//   *: warning: 'x<0>' is deprecated \[-Wdeprecated-declarations\]
//   *( )+([0-9]) | int &ref = x<0>;
//         |            ^~~~
//   *: note: declared here
//   *( )+([0-9]) | \[\[deprecated\]\] int x;
//         |                    ^
// pattern: clang++ 6.0, 7.0, 8.0
//   *: warning: 'x<0>' is deprecated \[-Wdeprecated-declarations\]
//   int &ref = x<0>;
//              ^
//   *: note: 'x<0>' has been explicitly marked deprecated here
//   \[\[deprecated\]\] int x;
//     ^
//   1 warning generated.

template<int>
[[deprecated]] int x;
int &ref = x<0>;
