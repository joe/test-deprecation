// anti-pattern: g++ 7.4, 8.3
//   *: warning: 'deprecated' attribute directive ignored \[-Wattributes\]
//    namespace \[\[deprecated\]\] NS {}
//                             *( )^?(~)
// anti-pattern: g++ 9.2
//   *: warning: 'deprecated' attribute directive ignored \[-Wattributes\]
//   *( )+([0-9]) | namespace \[\[deprecated\]\] NS {}
//         |                          ^~
// pattern: clang++ 6.0, 7.0, 8.0
//   *: warning: 'NS' is deprecated \[-Wdeprecated-declarations\]
//   using namespace NS;
//                   ^
//   *: note: 'NS' has been explicitly marked deprecated here
//   namespace \[\[deprecated\]\] NS {}
//               ^
//   1 warning generated.

namespace [[deprecated]] NS {}

using namespace NS;
