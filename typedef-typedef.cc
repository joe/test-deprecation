// pattern: g++ 7.4, 8.3
//   *: warning: 'S' is deprecated \[-Wdeprecated-declarations\]
//    typedef S Alias;
//              ^~~~~
//   *: note: declared here
//    \[\[deprecated\]\] typedef int S;
//                               ^
// pattern: g++ 9.2
//   *: warning: 'S' is deprecated \[-Wdeprecated-declarations\]
//   *( )+([0-9]) | typedef S Alias;
//         |           ^~~~~
//   *: note: declared here
//   *( )+([0-9]) | \[\[deprecated\]\] typedef int S;
//         |                            ^
// pattern: clang++ 6.0, 7.0, 8.0
//   *: warning: 'S' is deprecated \[-Wdeprecated-declarations\]
//   typedef S Alias;
//           ^
//   *: note: 'S' has been explicitly marked deprecated here
//   \[\[deprecated\]\] typedef int S;
//     ^
//   1 warning generated.

[[deprecated]] typedef int S;
typedef S Alias;
