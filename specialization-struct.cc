// pattern: g++ 7.4, 8.3
//   *: warning: 'S' is deprecated \[-Wdeprecated-declarations\]
//    typedef S<0> Alias;
//                 ^~~~~
//   *: note: declared here
//    struct \[\[deprecated\]\] S<0> {};
//                          ^~~~
// pattern: g++ 9.2
//   *: warning: 'S' is deprecated \[-Wdeprecated-declarations\]
//   *( )+([0-9]) | typedef S<0> Alias;
//         |              ^~~~~
//   *: note: declared here
//   *( )+([0-9]) | struct \[\[deprecated\]\] S<0> {};
//         |                       ^~~~
// pattern: clang++ 6.0, 7.0, 8.0
//   *: warning: 'S<0>' is deprecated \[-Wdeprecated-declarations\]
//   typedef S<0> Alias;
//           ^
//   *: note: 'S<0>' has been explicitly marked deprecated here
//   struct \[\[deprecated\]\] S<0> {};
//            ^
//   1 warning generated.

template<int>
struct S;
template<>
struct [[deprecated]] S<0> {};
typedef S<0> Alias;
