// pattern: g++ 7.4, 8.3 (double output)
//   *: warning: 'S<0>::x' is deprecated \[-Wdeprecated-declarations\]
//    int &ref0 = S<0>::x;
//                      ^
//   *: note: declared here
//    int S<i>::x;
//        ^~~~
// pattern: g++ 9.2
//   *: warning: 'S<0>::x' is deprecated \[-Wdeprecated-declarations\]
//   *( )+([0-9]) | int &ref0 = S<0>::x;
//         |                   ^
//   *: note: declared here
//   *( )+([0-9]) | int S<i>::x;
//         |     ^~~~
// pattern: clang++ 6.0, 7.0, 8.0
//   *: warning: 'x' is deprecated \[-Wdeprecated-declarations\]
//   int &ref0 = S<0>::x;
//                     ^
//   *: note: 'x' has been explicitly marked deprecated here
//     \[\[deprecated\]\] static int x;
//       ^
//   1 warning generated.

template<int>
struct S {
  [[deprecated]] static int x;
};
template<int i>
int S<i>::x;
int &ref0 = S<0>::x;
