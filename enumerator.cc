// pattern: g++ 7.4, 8.3
//   *: warning: 'A' is deprecated \[-Wdeprecated-declarations\]
//    int x = A;
//            ^
//   *: note: declared here
//    enum E { A \[\[deprecated\]\] = 42 };
//             ^
// pattern: g++ 9.2
//   *: warning: 'A' is deprecated \[-Wdeprecated-declarations\]
//   *( )+([0-9]) | int x = A;
//         |         ^
//   *: note: declared here
//   *( )+([0-9]) | enum E { A \[\[deprecated\]\] = 42 };
//         |          ^
// pattern: clang++ 6.0, 7.0, 8.0
//   *: warning: 'A' is deprecated \[-Wdeprecated-declarations\]
//   int x = A;
//           ^
//   *: note: 'A' has been explicitly marked deprecated here
//   enum E { A \[\[deprecated\]\] = 42 };
//                ^
//   1 warning generated.

enum E { A [[deprecated]] = 42 };
int x = A;
