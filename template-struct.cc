// pattern: g++ 7.4, 8.3
//   *: warning: 'template<int <anonymous> > struct S' is deprecated \[-Wdeprecated-declarations\]
//    typedef S<0> Alias;
//            ^
//   *: note: declared here
//    struct \[\[deprecated\]\] S;
//                          ^
// pattern: g++ 9.2
//   *: warning: 'template<int <anonymous> > struct S' is deprecated \[-Wdeprecated-declarations\]
//   *( )+([0-9]) | typedef S<0> Alias;
//         |         ^
//   *: note: declared here
//   *( )+([0-9]) | struct \[\[deprecated\]\] S;
//         |                       ^
// pattern: clang++ 6.0, 7.0, 8.0
//   *: warning: 'S<0>' is deprecated \[-Wdeprecated-declarations\]
//   typedef S<0> Alias;
//           ^
//   *: note: 'S<0>' has been explicitly marked deprecated here
//   struct \[\[deprecated\]\] S;
//            ^
//   1 warning generated.

template<int>
struct [[deprecated]] S;
typedef S<0> Alias;
