// anti-pattern: g++ 7.4, 8.3
// pattern: clang++ 6.0, 7.0, 8.0
//   *: warning: 'x' is deprecated \[-Wdeprecated-declarations\]
//   int &ref = S<i>::x;
//                    ^
//   *: note: in instantiation of variable template specialization 'ref<0>' requested here
//   int &ref0 = ref<0>;
//               ^
//   *: note: 'x' has been explicitly marked deprecated here
//     \[\[deprecated\]\] static int x;
//       ^
//   1 warning generated.

template<int>
struct S {
  [[deprecated]] static int x;
};
template<int i>
int S<i>::x;
template<int i>
int &ref = S<i>::x;

int &ref0 = ref<0>;
