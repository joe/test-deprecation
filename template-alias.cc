// pattern: g++ 7.4, 8.3
//   *: warning: 'using S = int' is deprecated \[-Wdeprecated-declarations\]
//    using Alias = S<i>;
//                  ^~~~
//   *: note: declared here
//    using S \[\[deprecated\]\] = int;
//                                ^
// pattern: g++ 9.2
//   *: warning: 'using S = int' is deprecated \[-Wdeprecated-declarations\]
//   *( )+([0-9]) | using Alias = S<i>;
//         |               *( )^?(~~~)
//   *: note: declared here
//   *( )+([0-9]) | using S \[\[deprecated\]\] = int;
//         |       ^
// anti-pattern: clang++ 6.0, 7.0, 8.0

template<int>
using S [[deprecated]] = int;
template<int i>
using Alias = S<i>;
