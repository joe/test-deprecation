TESTS := $(patsubst %.cc,%,$(wildcard *.cc))
RESULTS = $(addsuffix .result, $(TESTS))

CLEANFILES =

export CXX
override export LC_ALL=C

.PHONY: all
all: result.json

.PHONY: cat
cat: result.json
	cat result.json

.PHONY: suggestions
suggestions: $(RESULTS)
	@for t in $(TESTS); do						\
	  if ! echo true | cmp - $$t.result >/dev/null 2>&1		\
	     && ! echo false | cmp - $$t.result >/dev/null 2>&1; then	\
	    echo "$$t.cc:";						\
	    ./suggest-pattern "$$t";					\
	  fi;								\
	done

result.json: testset.stamp $(RESULTS)
	$(ifsilent)echo "jq >result.json"
	jq '{ key: input_filename|sub("\\.result$$"; ""), value: . }' \
	    $(RESULTS) >result.tmp
	jq -s '. | from_entries' result.tmp >result.json
	rm result.tmp
CLEANFILES += result.tmp result.json

%.result: %.cc
	$(ifsilent)echo test-deprecation $*
	./test-deprecation $< >$@.tmp 2>$@.log || { cat $@.log; false; }
	mv $@.tmp $@
	echo "$*: $$(cat $@)"
CLEANFILES += *.result *.result.log

# dependency tracking for the set of test
# update testset.stamp each time the contents of $(TESTS) changes
-include testset.phony
testset.phony:
	@echo $(TESTS) | cmp - testset.stamp >/dev/null 2>&1 \
	  || echo $(TESTS) >testset.stamp
.PHONY: testset.phony
CLEANFILES += testset.stamp

.PHONY: clean
clean:
	rm -f $(CLEANFILES)

define hash
#
endef
ifsilent = @$(if $(VERBOSE),$(hash))
.SILENT: $(if $(VERBOSE),dummy)
