// Note: the below message appears twice in g++ 8.3, we accept that by the
// first * (which usually is only supposed to match the file name

// pattern: g++ 7.4, 8.3
//   *: In function 'void g()':
//   *: warning: 'void f(T) \[with T = int\]' is deprecated \[-Wdeprecated-declarations\]
//      f(0);
//         ^
//   *: note: declared here
//    \[\[deprecated\]\] void f(T);
//                        ^
// pattern: g++ 9.2
//   *: In function 'void g()':
//   *: warning: 'void f(T) \[with T = int\]' is deprecated \[-Wdeprecated-declarations\]
//   *( )+([0-9]) |   f(0);
//         |      ^
//   *: note: declared here
//   *( )+([0-9]) | \[\[deprecated\]\] void f(T);
//         |                     ^
// pattern: clang++ 6.0, 7.0, 8.0
//   *: warning: 'f<int>' is deprecated \[-Wdeprecated-declarations\]
//     f(0);
//     ^
//   *: note: 'f<int>' has been explicitly marked deprecated here
//   \[\[deprecated\]\] void f(T);
//     ^
//   1 warning generated.

template<class T>
[[deprecated]] void f(T);
void g()
{
  f(0);
}
